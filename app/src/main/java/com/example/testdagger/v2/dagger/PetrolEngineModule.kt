package com.example.testdagger.v2.dagger

import com.example.testdagger.v2.car.Engine
import com.example.testdagger.v2.car.PetrolEngine
import dagger.Binds
import dagger.Module


@Module
abstract class PetrolEngineModule {

    @Binds
    abstract fun bindEngine(engine: PetrolEngine): Engine
}