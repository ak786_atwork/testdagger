package com.example.testdagger.v2.dagger

import com.example.testdagger.MainActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Named

@Component (modules = [WheelsModule::class, DieselEngineModule::class])
public interface CarComponent{

//    fun getCar(): Car
    fun inject(mainActivity: MainActivity)


    @Component.Builder
    public interface Builder {

        @BindsInstance
        public fun horsePower(@Named("horse power") horsePower: Int): Builder

        @BindsInstance
        public fun engineCapacity(@Named("engine capacity") engineCapacity: Int): Builder

        public fun build(): CarComponent
    }

}