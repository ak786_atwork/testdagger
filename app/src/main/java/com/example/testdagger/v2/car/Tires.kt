package com.example.testdagger.v2.car

import android.util.Log

class Tires {
    //we don't own this class, so we can't annotate it with @inject

    private val TAG: String = "Debug: CAR"

    fun inflate() {
        Log.d(TAG, "tires inflated")
    }

}
