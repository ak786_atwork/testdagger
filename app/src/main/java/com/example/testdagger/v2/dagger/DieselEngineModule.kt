package com.example.testdagger.v2.dagger

import com.example.testdagger.v2.car.DieselEngine
import com.example.testdagger.v2.car.Engine
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Inject

@Module
abstract class DieselEngineModule {

    @Binds
    abstract fun bindEngine(engine: DieselEngine): Engine

}