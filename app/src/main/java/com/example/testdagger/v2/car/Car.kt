package com.example.testdagger.v2.car

import android.util.Log
import javax.inject.Inject

class Car {

    private val TAG: String = "Debug: CAR"

    private lateinit var engine: Engine
    private lateinit var wheels: Wheels

    @Inject
    constructor(engine: Engine, wheels: Wheels) {
        this.engine = engine
        this.wheels = wheels
    }

    @Inject
    fun enableRemote(remote: Remote) {
        remote.setListener(this)
    }

    fun drive() {
        engine.start()
        Log.d(TAG, "driving...")
    }
}