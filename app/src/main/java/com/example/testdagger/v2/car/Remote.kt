package com.example.testdagger.v2.car

import android.util.Log
import javax.inject.Inject

class Remote {
    private val TAG: String = "Debug: CAR"

    @Inject
    constructor()

    fun setListener(car: Car){
        Log.d(TAG, "remote connected..")
    }
}