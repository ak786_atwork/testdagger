package com.example.testdagger.v2.car


class Wheels {
    //we don't own this class so we can't annotate it with @inject


    private var rims: Rims
    private var tires: Tires

    constructor(rims: Rims, tires: Tires) {
        this.rims = rims
        this.tires = tires
    }
}