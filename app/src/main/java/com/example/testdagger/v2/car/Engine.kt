package com.example.testdagger.v2.car

interface Engine {
    fun start()
}