package com.example.testdagger.v2.car

import android.util.Log
import javax.inject.Inject
import javax.inject.Named

class DieselEngine: Engine {
    private val TAG: String = "Debug: CAR"

    private var horsePower:Int = 0
    private var engineCapacity:Int = 0

    @Inject
    constructor(@Named("horse power") horsePower: Int, @Named("engine capacity") engineCapacity: Int) {
        this.horsePower = horsePower
        this.engineCapacity = engineCapacity
    }

    override fun start() {
        Log.d(TAG, "diesel engine started with horse power $horsePower and engine capacity $engineCapacity")
    }
}