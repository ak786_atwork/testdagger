package com.example.testdagger

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.testdagger.v2.car.Car
import com.example.testdagger.v2.dagger.DaggerCarComponent
import com.example.testdagger.v2.dagger.DieselEngineModule
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject lateinit var car: Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val carComponent = DaggerCarComponent.builder()
            .horsePower(2000)
            .engineCapacity(3000)
            .build()
        carComponent.inject(this)

        car.drive()
    }
}
